// Fill out your copyright notice in the Description page of Project Settings.


#include "NewActorComponent.h"
#include "GameFramework/Actor.h"

// Sets default values for this component's properties
UNewActorComponent::UNewActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UNewActorComponent::BeginPlay()
{
	Super::BeginPlay();

	FString Log = TEXT("Hello!");
	FString Name = GetOwner()->GetName();
	FString Position = GetOwner()->GetActorLocation().ToString();

	UE_LOG(LogTemp, Warning, TEXT("%s, my name is %s"), *Log, *Name);
	UE_LOG(LogTemp, Warning, TEXT("I am positioned at %s"), *Position);
}


// Called every frame
void UNewActorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

