// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"
#include "GameFramework/Actor.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"

#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber()
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UGrabber::BeginPlay()
{
    Super::BeginPlay();

    FindPlayerController();
    FindPhysicsHandle();
    SetUpInputComponent();
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);


    if (PhysicHandle && PhysicHandle->GrabbedComponent)
    {
        PhysicHandle->SetTargetLocation(GetLineTraceEnd(DistanceWhenGrabbed, HeightWhenGrabbed));
    }
}

void UGrabber::Grab()
{
    FHitResult HitResult = GetFirstPhysicBodyInReach();
    AActor* ActorHit = HitResult.GetActor();
    if (ActorHit)
    {
        if (!PhysicHandle) return;
        UPrimitiveComponent* ComponentToGrab = HitResult.GetComponent();
        PhysicHandle->GrabComponentAtLocation(ComponentToGrab, NAME_None, GetLineTraceEnd(Reach, 0));
    }
}

void UGrabber::Release()
{
    if (!PhysicHandle) return;
    PhysicHandle->ReleaseComponent();
}

void UGrabber::FindPhysicsHandle()
{
    const AActor* Owner = GetOwner();
    PhysicHandle = Owner->FindComponentByClass<UPhysicsHandleComponent>();
    if (!PhysicHandle)
    {
        UE_LOG(LogTemp, Error, TEXT("The component Grabber inside %s doesn't have a PhysicsHandleComponent"),
               *Owner->GetName());
    }
}

void UGrabber::SetUpInputComponent()
{
    const AActor* Owner = GetOwner();

    InputComponent = Owner->FindComponentByClass<UInputComponent>();
    if (!InputComponent)
    {
        UE_LOG(LogTemp, Error, TEXT("The component Grabber inside %s doesn't have a InputComponent"),
               *Owner->GetName());
    } else
    {
        InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
        InputComponent->BindAction("Grab", IE_Released, this, &UGrabber::Release);
    }
}

FHitResult UGrabber::GetFirstPhysicBodyInReach()
{

    FHitResult HitResult;
    FCollisionQueryParams TraceParams(FName(TEXT("")), false, GetOwner());
    GetWorld()->LineTraceSingleByObjectType(
            OUT HitResult,
            GetPlayerWorldPosition(),
            GetLineTraceEnd(Reach, 0),
            FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
            TraceParams);

    return HitResult;
}

FVector UGrabber::GetLineTraceEnd(float TraceReach, float Height)
{
    FVector PlayerViewPointLocation;
    FRotator PlayerViewPointRotation;
    PlayerController->GetPlayerViewPoint(OUT PlayerViewPointLocation, OUT PlayerViewPointRotation);
    FVector LineTraceEnd = PlayerViewPointLocation + PlayerViewPointRotation.Vector() * TraceReach;
    LineTraceEnd.Z += Height;
    return LineTraceEnd;
}

FVector UGrabber::GetPlayerWorldPosition()
{
    FVector PlayerViewPointLocation;
    FRotator PlayerViewPointRotation;

    PlayerController->GetPlayerViewPoint(OUT PlayerViewPointLocation, OUT PlayerViewPointRotation);

    return PlayerViewPointLocation;
}

void UGrabber::FindPlayerController()
{
    PlayerController = GetWorld()->GetFirstPlayerController();
    if (!PlayerController)
    {
        UE_LOG(LogTemp, Error, TEXT("The grabber in %s can't find the player controller"), *GetOwner()->GetName());
    }
}