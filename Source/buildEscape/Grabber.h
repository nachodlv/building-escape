// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameFramework/PlayerController.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:

    UPROPERTY(EditAnywhere)
    float Reach = 100.f;

    UPROPERTY(EditAnywhere)
    float DistanceWhenGrabbed = 50.f;

    UPROPERTY(EditAnywhere)
    float HeightWhenGrabbed = 10.f;

    UPROPERTY()
    APlayerController* PlayerController = nullptr;
    UPROPERTY()
    UPhysicsHandleComponent* PhysicHandle = nullptr;
    UPROPERTY()
    UInputComponent* InputComponent = nullptr;

    void Grab();
    void Release();
    void FindPhysicsHandle();
    void SetUpInputComponent();

    // Returns the first Actor within reach whith the physic body
    // If no Actor is in reach, it will return a nullptr
    FHitResult GetFirstPhysicBodyInReach();

    // Returns the reach vector
    FVector GetLineTraceEnd(float TraceReach, float Height);

    FVector GetPlayerWorldPosition();

    void FindPlayerController();
};
