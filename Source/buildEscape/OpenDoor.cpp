// Fill out your copyright notice in the Description page of Project Settings.

#include "OpenDoor.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "GameFramework/PlayerController.h"
#include "Math/UnrealMathUtility.h"
#include "Components/PrimitiveComponent.h"

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
    Super::BeginPlay();
    const AActor* Owner = GetOwner();
    CurrentYaw = Owner->GetActorRotation().Yaw;
    TargetYaw += CurrentYaw;
    InitialYaw = CurrentYaw;

    FindAudioComponent();

    if (!PressurePlate)
    {
        UE_LOG(LogTemp, Error, TEXT("The actor %s has no pressure plate assigned"), *Owner->GetName());
    }
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    if (GetTotalMassInPressurePlate() >= MassRequiredToOpen)
    {
        if(!Opening) {
            if(AudioComponent) AudioComponent->Play();
            Opening = true;
        }
        TriggerDoor(DeltaTime, TargetYaw);
        DoorLastOpened = GetWorld()->GetTimeSeconds();
    } else if ((GetWorld()->GetTimeSeconds() - DoorLastOpened) > DoorCloseDelayed)
    {
        if(Opening) {
            if(AudioComponent) AudioComponent->Play();
            Opening = false;
        }
        TriggerDoor(DeltaTime, InitialYaw);
    }
}

void UOpenDoor::TriggerDoor(float DeltaTime, float Yaw)
{
    const float NewYaw = FMath::FInterpTo(CurrentYaw, Yaw, DeltaTime, OpenSpeed);
    FRotator Rotation(0.f, NewYaw, 0.f);
    CurrentYaw = NewYaw;
    GetOwner()->SetActorRotation(Rotation);
}

float UOpenDoor::GetTotalMassInPressurePlate() const
{
    float TotalMass = 0.f;
    TArray<AActor*> OverlappingActors;
    if (!PressurePlate) return 0.f;
    PressurePlate->GetOverlappingActors(OUT OverlappingActors);
    for (auto* Actor: OverlappingActors)
    {
        auto* PrimitiveComponent = Actor->FindComponentByClass<UPrimitiveComponent>();
        if (PrimitiveComponent)
        {
            TotalMass += PrimitiveComponent->GetMass();
        }
    }

    return TotalMass;
}

void UOpenDoor::FindAudioComponent()
{
    AudioComponent = GetOwner()->FindComponentByClass<UAudioComponent>();
    if (!AudioComponent)
    {
        UE_LOG(LogTemp, Error, TEXT("OpenDoor in %s didn't found an AudioComponent"), *GetOwner()->GetName());
    }
}

