// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "Components/AudioComponent.h"
#include "OpenDoor.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))

class BUILDESCAPE_API UOpenDoor : public UActorComponent
{
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
    UOpenDoor();

protected:
    // Called when the game starts
    virtual void BeginPlay() override;

public:
    // Called every frame
    virtual void
    TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
    UPROPERTY(EditAnywhere)
    float TargetYaw = -90.f;

    UPROPERTY(EditAnywhere)
    float OpenSpeed = 2.f;

    UPROPERTY(EditAnywhere)
    ATriggerVolume* PressurePlate = nullptr;

    UPROPERTY(EditAnywhere)
    float DoorCloseDelayed = 1.f;

    UPROPERTY(EditAnywhere)
    float MassRequiredToOpen = 50.f;

    UPROPERTY()
    UAudioComponent* AudioComponent = nullptr;

    float DoorLastOpened = 0.f;
    float CurrentYaw;
    float InitialYaw;
    bool Opening = false;

    void TriggerDoor(float DeltaTime, float Yaw);

    float GetTotalMassInPressurePlate() const;

    void FindAudioComponent();
};
